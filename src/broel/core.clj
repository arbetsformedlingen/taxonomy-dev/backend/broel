(ns broel.core
  (:gen-class)
  (:require
   [cheshire.core :as c]
   [chime.core :as chime]
   [clj-http.client :as client]
   [clojure.java.io :as io]
   [clojure.tools.logging :as log]
   [cprop.core :refer [load-config]]
   [cprop.source :refer [from-resource]])
  (:import [java.time Duration Instant]))

(def conf (load-config :merge [(if (.exists (io/file "resources/dev-config.edn"))
                                 (from-resource "dev-config.edn")
                                 (from-resource "config.edn"))]))

(def query

  {"query" {"bool" {"must" [{"query_string" {"default_field" "kubernetes.namespace",
                                             "query" "taxonomy AND api AND gitops"}}
                            ,
                            {"query_string" {"default_field" "message",
                                             "query" "Error OR Exception OR clojure.lang.ExceptionInfo OR (at AND NOT 200) OR (503 AND NOT 200)"}}]

                    "filter" [{"range" {"@timestamp" {"gt" "now-1m",
                                                      "lte" "now"}}}]}}

   "size" "1000"})

(defn query-elk! []
  (client/get (str (:elk-base-url conf) "filebeat-*/_search")
              {:as :json-strict
               :basic-auth [(:elk-user conf) (:elk-password conf)]
               :body (c/encode query)
               :content-type :json
               :accept :json}))

(defn parse-elk-response [response]
  (let [hits (get-in response [:body :hits :hits])
        messages (map #(str (get-in (first hits) [:_source :kubernetes :pod :name])
                            " - " (get-in % [:_source :message])) hits)]
    messages))

(defn post-to-mattermost! [message]
  (client/post (:mattermost-api-url conf)
               {:body (c/encode {:channel_id (:mattermost-channel-id conf)
                                 :message message})
                :content-type :json
                :headers {"Authorization" (str "Bearer " (:mattermost-token conf))}}))

(defn send-messages! [messages]
  (let [messages-as-string (reduce (fn [acc element]  (str acc element "\n")) "" (reverse messages))]
    (post-to-mattermost! messages-as-string)))

(defn handle-messages [messages]
  (if (empty? messages)
    (log/info "No messages to send")
    (log/info (:status (send-messages! messages)))))

(defn tasks-fn [arg]
  (let [response (query-elk!)
        messages (parse-elk-response response)
        success (handle-messages messages)]
    true))

(defn -main
  "Call elastic repeat and check for errors and exceptions"
  [& args]
  (chime/chime-at (chime/periodic-seq (Instant/now) (Duration/ofMinutes 1))
                  tasks-fn
                  {:error-handler (fn [e]
                                    (log/error e))}))
