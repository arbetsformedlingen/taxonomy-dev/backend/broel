# BRÖL!

A ELK-stack log alert made in pure clojure!

![Image of the awesome elk](elk.png)

## Installation/Deployment

Deployed in the Prod cluster in project [broel-elk-alert](https://console-openshift-console.prod.services.jtech.se/k8s/cluster/projects/broel-elk-alert)

## Rollout

Goto the broel Build Config and run Start Build, then a new pod is built from master and deployed in Prod.

## Options

FIXME: listing of options this app accepts.

## Examples

...

### Bugs

...

### Any Other Sections
### That You Think
### Might be Useful

## License

Copyright © 2020 JobTech

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
