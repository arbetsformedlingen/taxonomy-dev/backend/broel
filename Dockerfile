FROM clojure:temurin-18-lein-alpine as builder

COPY . /

RUN pwd

WORKDIR /

RUN lein uberjar

FROM adoptopenjdk:11

COPY --from=builder target/uberjar/broel-0.1.0-SNAPSHOT-standalone.jar /broel/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/broel/app.jar"]
