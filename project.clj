(defproject broel "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [cprop "0.1.17"]
                 [clj-http "3.11.0"]
                 [jarohen/chime "0.3.2"]
                 [clojurewerkz/elastisch "5.0.0-beta1"]
                 [org.clojure/core.async "1.3.610"]
                 [clj-time "0.15.2"]

                 [cheshire "5.10.0"]

                 ]

  :plugins [[com.livingsocial/lein-dependency-check "1.1.4"]
            [lein-cljfmt "0.6.6"]
            [lein-ancient "0.6.15"]
            [lein-nsorg "0.3.0"]
            [jonase/eastwood "0.3.6"]]

  :main ^:skip-aot broel.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
